# ViaLunar
- A ViaVersion implementation for Lunar Client.

![](https://i.imgur.com/3kSAm8X.jpg)
![](https://i.imgur.com/cRSu1li.jpg)

## How to use
1. Download [Weave Manager](https://github.com/exejar/Weave-Manager).
2. Download [ViaLunar](https://gitlab.com/Candicey-Weave/viaversion-lunar/-/releases).
3. Download the corresponding version of [Zenith Core](https://gitlab.com/candicey-weave/zenith-core/-/releases) that is mentioned in the release notes.

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.
- [ViaVersion/ViaBackwards/ViaRewind](https://github.com/ViaVersion) - For the base of the code.
- [ViaForge](https://github.com/FlorianMichael/ViaForge) - For the base of the code.

Click <a href="https://gitlab.com/Candicey-Weave/viaversion-lunar/-/releases">here</a> for releases.