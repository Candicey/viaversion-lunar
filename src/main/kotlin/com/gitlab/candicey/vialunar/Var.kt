package com.gitlab.candicey.vialunar

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

val LOGGER: Logger = LogManager.getLogger("ViaLunar")