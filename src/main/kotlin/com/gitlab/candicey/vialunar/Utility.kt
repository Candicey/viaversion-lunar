package com.gitlab.candicey.vialunar

import org.objectweb.asm.Type

inline fun <reified T : Any> internalNameOf(): String = Type.getInternalName(T::class.java)