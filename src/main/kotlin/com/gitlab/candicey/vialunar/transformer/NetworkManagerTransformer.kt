package com.gitlab.candicey.vialunar.transformer

import com.gitlab.candicey.vialunar.extension.insertBeforeReturn
import com.gitlab.candicey.vialunar.extension.named
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode

object NetworkManagerTransformer : Hook("net/minecraft/network/NetworkManager") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.named("setCompressionTreshold").instructions.insertBeforeReturn(asm {
            aload(0)
            getfield("net/minecraft/network/NetworkManager", "channel", "Lio/netty/channel/Channel;")
            invokeinterface("io/netty/channel/Channel", "pipeline", "()Lio/netty/channel/ChannelPipeline;")
            new("de/florianmichael/vialoadingbase/event/PipelineReorderEvent")
            dup
            invokespecial("de/florianmichael/vialoadingbase/event/PipelineReorderEvent", "<init>", "()V")
            invokeinterface(
                "io/netty/channel/ChannelPipeline",
                "fireUserEventTriggered",
                "(Ljava/lang/Object;)Lio/netty/channel/ChannelPipeline;"
            )
            pop
        })

        cfg.computeFrames()
    }
}