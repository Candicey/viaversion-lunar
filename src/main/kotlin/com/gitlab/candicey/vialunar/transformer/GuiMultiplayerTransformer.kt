package com.gitlab.candicey.vialunar.transformer

import com.gitlab.candicey.vialunar.extension.insertBeforeReturn
import com.gitlab.candicey.vialunar.extension.named
import com.gitlab.candicey.vialunar.gui.GuiProtocolSelector
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode

object GuiMultiplayerTransformer : Hook("net/minecraft/client/gui/GuiMultiplayer") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.named("initGui").instructions.insertBeforeReturn(asm {
            new("net/minecraft/client/gui/GuiButton")
            dup
            sipush(1337)
            sipush(5)
            sipush(6)
            sipush(98)
            sipush(20)
            ldc("ViaLunar")
            invokespecial("net/minecraft/client/gui/GuiButton", "<init>", "(IIIIILjava/lang/String;)V")
            aload(0)
            getfield("net/minecraft/client/gui/GuiMultiplayer", "buttonList", "Ljava/util/List;")
            swap
            invokeinterface("java/util/List", "add", "(Ljava/lang/Object;)Z")
        })

        node.methods.named("actionPerformed").instructions.insert(asm {
            aload(0)
            aload(1)
            invokestatic(
                "com/gitlab/candicey/vialunar/transformer/GuiMultiplayerTransformer",
                "onActionPerformed",
                "(Ljava/lang/Object;Lnet/minecraft/client/gui/GuiButton;)V"
            )
        })
    }

    @JvmStatic
    fun onActionPerformed(instance: Any, guiButton: GuiButton) {
        if (guiButton.id == 1337) {
            Minecraft.getMinecraft().displayGuiScreen(GuiProtocolSelector(instance as GuiScreen))
        }
    }
}