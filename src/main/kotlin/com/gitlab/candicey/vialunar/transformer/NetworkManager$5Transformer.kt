package com.gitlab.candicey.vialunar.transformer

import com.gitlab.candicey.vialunar.extension.named
import com.viaversion.viaversion.connection.UserConnectionImpl
import com.viaversion.viaversion.protocol.ProtocolPipelineImpl
import de.florianmichael.vialoadingbase.ViaLoadingBase
import de.florianmichael.vialoadingbase.netty.NettyConstants
import de.florianmichael.vialoadingbase.netty.VLBViaDecodeHandler
import de.florianmichael.vialoadingbase.netty.VLBViaEncodeHandler
import io.netty.channel.Channel
import io.netty.channel.socket.SocketChannel
import net.minecraft.realms.RealmsSharedConstants
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.ClassNode

object `NetworkManager$5Transformer` : Hook("net/minecraft/network/NetworkManager$5") {
        @JvmStatic
        fun onInitChannel(channel: Channel) {
            if (channel is SocketChannel && ViaLoadingBase.getClassWrapper().targetVersion.version != RealmsSharedConstants.NETWORK_PROTOCOL_VERSION) {
                val user = UserConnectionImpl(channel, true)
                ProtocolPipelineImpl(user)

                channel.pipeline()
                    .addBefore("encoder", NettyConstants.HANDLER_ENCODER_NAME, VLBViaEncodeHandler(user))
                    .addBefore("decoder", NettyConstants.HANDLER_DECODER_NAME, VLBViaDecodeHandler(user))
            }
        }

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.named("initChannel").instructions.let {
            it.insertBefore(it.findLast { insnNode -> insnNode.opcode == RETURN }, asm {
                aload(1)
                invokestatic(
                    "com/gitlab/candicey/vialunar/transformer/NetworkManager$5Transformer",
                    "onInitChannel",
                    "(Lio/netty/channel/Channel;)V"
                )
            })
        }

        cfg.computeFrames()
    }
}