package com.gitlab.candicey.vialunar.child

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.gui.Gui
import net.minecraft.client.gui.GuiSlot
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.Tessellator
import net.minecraft.client.renderer.vertex.DefaultVertexFormats

abstract class GuiSlotChild(
    mc: Minecraft,
    width: Int,
    height: Int,
    top: Int,
    bottom: Int,
    slotHeight: Int
) : GuiSlot(
    mc,
    width,
    height,
    top,
    bottom,
    slotHeight
) {
    val zLevel: Float = 0F

    fun drawCenteredString(fontRenderer: FontRenderer, text: String, x: Int, y: Int, color: Int) {
        fontRenderer.drawStringWithShadow(
            text,
            (x - fontRenderer.getStringWidth(text) / 2).toFloat(),
            y.toFloat(),
            color
        )
    }

    fun drawDefaultBackground() {
        drawWorldBackground(0)
    }

    fun drawWorldBackground(tint: Int) {
        if (mc.theWorld != null) {
            drawGradientRect(0, 0, width, height, -1072689136, -804253680)
        } else {
            drawBackground(tint)
        }
    }

    fun drawBackground(tint: Int) {
        GlStateManager.disableLighting()
        GlStateManager.disableFog()
        val tessellator = Tessellator.getInstance()
        val worldrenderer = tessellator.worldRenderer
        mc.textureManager.bindTexture(Gui.optionsBackground)
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f)
        val f = 32.0f
        worldrenderer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR)
        worldrenderer.pos(0.0, height.toDouble(), 0.0).tex(0.0, (height.toFloat() / 32.0f + tint.toFloat()).toDouble())
            .color(64, 64, 64, 255).endVertex()
        worldrenderer.pos(width.toDouble(), height.toDouble(), 0.0)
            .tex((width.toFloat() / 32.0f).toDouble(), (height.toFloat() / 32.0f + tint.toFloat()).toDouble())
            .color(64, 64, 64, 255).endVertex()
        worldrenderer.pos(width.toDouble(), 0.0, 0.0).tex((width.toFloat() / 32.0f).toDouble(), tint.toDouble())
            .color(64, 64, 64, 255).endVertex()
        worldrenderer.pos(0.0, 0.0, 0.0).tex(0.0, tint.toDouble()).color(64, 64, 64, 255).endVertex()
        tessellator.draw()
    }

    protected open fun drawGradientRect(left: Int, top: Int, right: Int, bottom: Int, startColor: Int, endColor: Int) {
        val f = (startColor shr 24 and 255).toFloat() / 255.0f
        val f1 = (startColor shr 16 and 255).toFloat() / 255.0f
        val f2 = (startColor shr 8 and 255).toFloat() / 255.0f
        val f3 = (startColor and 255).toFloat() / 255.0f
        val f4 = (endColor shr 24 and 255).toFloat() / 255.0f
        val f5 = (endColor shr 16 and 255).toFloat() / 255.0f
        val f6 = (endColor shr 8 and 255).toFloat() / 255.0f
        val f7 = (endColor and 255).toFloat() / 255.0f
        GlStateManager.disableTexture2D()
        GlStateManager.enableBlend()
        GlStateManager.disableAlpha()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.shadeModel(7425)
        val tessellator = Tessellator.getInstance()
        val worldrenderer = tessellator.worldRenderer
        worldrenderer.begin(7, DefaultVertexFormats.POSITION_COLOR)
        worldrenderer.pos(right.toDouble(), top.toDouble(), zLevel.toDouble()).color(f1, f2, f3, f).endVertex()
        worldrenderer.pos(left.toDouble(), top.toDouble(), zLevel.toDouble()).color(f1, f2, f3, f).endVertex()
        worldrenderer.pos(left.toDouble(), bottom.toDouble(), zLevel.toDouble()).color(f5, f6, f7, f4).endVertex()
        worldrenderer.pos(right.toDouble(), bottom.toDouble(), zLevel.toDouble()).color(f5, f6, f7, f4).endVertex()
        tessellator.draw()
        GlStateManager.shadeModel(7424)
        GlStateManager.disableBlend()
        GlStateManager.enableAlpha()
        GlStateManager.enableTexture2D()
    }
}