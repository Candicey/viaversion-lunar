package com.gitlab.candicey.vialunar.gui

import com.gitlab.candicey.vialunar.ViaLunarMain
import com.gitlab.candicey.vialunar.child.GuiSlotChild
import com.mojang.realmsclient.gui.ChatFormatting
import de.florianmichael.vialoadingbase.ViaLoadingBase
import de.florianmichael.vialoadingbase.platform.InternalProtocolList
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import java.io.IOException

class GuiProtocolSelector(val parent: GuiScreen) : GuiScreen() {
    lateinit var list: SlotList

    override fun initGui() {
        super.initGui()
        buttonList.add(
            GuiButton(
                1, width / 2 - 100, height - 27, 200,
                20, "Back"
            )
        )
        list = SlotList(mc, width, height, 32, height - 32, 10)
    }

    @Throws(IOException::class)
    override fun actionPerformed(guiButton: GuiButton) {
        list.actionPerformed(guiButton)
        if (guiButton.id == 1) mc.displayGuiScreen(parent)
    }

    @Throws(IOException::class)
    override fun handleMouseInput() {
        list.handleMouseInput()
        super.handleMouseInput()
    }

    override fun keyTyped(p0: Char, p1: Int) {
        if (p1 == Keyboard.KEY_ESCAPE) {
            mc.displayGuiScreen(parent)
            return
        }

        super.keyTyped(p0, p1)
    }

    override fun drawScreen(p_drawScreen_1_: Int, p_drawScreen_2_: Int, p_drawScreen_3_: Float) {
        list.drawScreen(p_drawScreen_1_, p_drawScreen_2_, p_drawScreen_3_)
        GL11.glPushMatrix()
        GL11.glScalef(2.0f, 2.0f, 2.0f)
        drawCenteredString(fontRendererObj, ChatFormatting.GOLD.toString() + "ViaLunar", width / 4, 6, 16777215)
        GL11.glPopMatrix()
        if (ViaLunarMain.watermark) {
            drawString(fontRendererObj, "Ported to Lunar Client by Candicey", 1, 11, -1)
            drawString(fontRendererObj, "Gitlab: https://gitlab.com/Candicey/ViaLunar", 1, 21, -1)
            drawString(fontRendererObj, "ViaForge by EnZaXD/Flori2007", 1, 31, -1)
            drawString(fontRendererObj, "Discord: EnZaXD#6257", 1, 41, -1)
        }
        super.drawScreen(p_drawScreen_1_, p_drawScreen_2_, p_drawScreen_3_)
    }

    class SlotList(
        mc: Minecraft,
        width: Int,
        height: Int,
        top: Int,
        bottom: Int,
        slotHeight: Int
    ) : GuiSlotChild(
        mc,
        width,
        height,
        top,
        bottom,
        slotHeight
    ) {
        override fun getSize(): Int {
            return InternalProtocolList.getProtocols().size
        }

        override fun elementClicked(i: Int, b: Boolean, i1: Int, i2: Int) {
            ViaLoadingBase.getClassWrapper().reload(InternalProtocolList.getProtocols()[i])
        }

        override fun isSelected(i: Int): Boolean {
            return false
        }

        override fun drawBackground() {
            drawDefaultBackground()
        }

        override fun drawSlot(i: Int, i1: Int, i2: Int, i3: Int, i4: Int, i5: Int) {
            val version = InternalProtocolList.getProtocols()[i]
            drawCenteredString(
                mc.fontRendererObj,
                (if (ViaLoadingBase.getClassWrapper().targetVersion.version == version.version) ChatFormatting.GREEN.toString() else ChatFormatting.DARK_RED.toString()) + version.name,
                width / 2,
                i2,
                -1
            )
        }
    }
}