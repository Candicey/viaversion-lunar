package com.gitlab.candicey.vialunar.extension

import com.gitlab.candicey.vialunar.internalNameOf
import net.weavemc.loader.api.event.Event
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.util.InsnBuilder
import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.InsnList

fun InsnList.insertBeforeReturn(insnList: InsnList) {
    this.filter { it.opcode == RETURN }
        .forEach { this.insertBefore(it, insnList) }
}

inline fun <reified T : Any> InsnBuilder.getSingleton() =
    getstatic(internalNameOf<T>(), "INSTANCE", "L${internalNameOf<T>()};")

fun InsnBuilder.callEvent() {
    invokestatic(
        internalNameOf<EventBus>(),
        "callEvent",
        "(L${internalNameOf<Event>()};)V"
    )
}