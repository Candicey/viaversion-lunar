package com.gitlab.candicey.vialunar

import com.gitlab.candicey.vialunar.transformer.GuiMultiplayerTransformer
import com.gitlab.candicey.vialunar.transformer.GuiScreenServerListTransformer
import com.gitlab.candicey.vialunar.transformer.`NetworkManager$5Transformer`
import com.gitlab.candicey.vialunar.transformer.NetworkManagerTransformer
import com.gitlab.candicey.zenithcore.extension.add
import com.gitlab.candicey.zenithcore.helper.HookManagerHelper
import de.florianmichael.vialoadingbase.ViaLoadingBase
import net.minecraft.client.Minecraft
import net.minecraft.realms.RealmsSharedConstants
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent

class ViaLunarMain : ModInitializer {
    companion object {
        var watermark = true
    }

    override fun preInit() {
        EventBus.subscribe(StartGameEvent.Pre::class.java) {
            LOGGER.info("[ViaLunar] Initialising ViaLunar...")

            if (RealmsSharedConstants.NETWORK_PROTOCOL_VERSION != 47) {
                LOGGER.warn("[ViaLunar] ViaLunar is not compatible with this version of Minecraft!")
                LOGGER.warn("[ViaLunar] Please use Minecraft 1.8.9!")
            } else {
                HookManagerHelper.hooks.add(
                    NetworkManagerTransformer,
                    `NetworkManager$5Transformer`,
                    GuiMultiplayerTransformer,
                    GuiScreenServerListTransformer,
                )
            }

            LOGGER.info("[ViaLunar] Initialised ViaLunar!")
        }

        EventBus.subscribe(StartGameEvent.Post::class.java) {
            ViaLoadingBase
                .ViaLoadingBaseBuilder
                .create()
                .runDirectory(Minecraft.getMinecraft().mcDataDir)
                .nativeVersion(RealmsSharedConstants.NETWORK_PROTOCOL_VERSION)
                .forceNativeVersionCondition { Minecraft.getMinecraft().isSingleplayer }
                .build()

            ViaLunarMain::class.java.getResourceAsStream("/vialunar/watermark.txt")?.let {
                watermark = it.readAllBytes().toString(Charsets.UTF_8).toBooleanStrictOrNull() ?: true
            }

            LOGGER.info("[ViaLunar] ViaLoadingBase started!")
        }
    }
}