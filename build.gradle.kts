import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version ("1.8.0")
    id("com.github.weave-mc.weave") version ("8b70bcc707")
}

group = "com.gitlab.candicey.vialunar"
version = "3.0.0"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://repo.viaversion.com/")
}

dependencies {
    testImplementation(kotlin("test"))

//    compileOnly("com.github.Weave-MC:Weave-Loader:94d4e893")

    compileOnly("com.viaversion:viaversion:4.6.0")
    compileOnly("com.viaversion:viabackwards:4.6.1-SNAPSHOT")
    compileOnly("com.viaversion:viarewind-core:2.0.3")

    compileOnly("com.github.FlorianMichael:ViaLoadingBase:6dad0a2561")

    compileOnly("com.github.Weave-MC:Weave-Loader:70bd82faa6")
    compileOnly("com.gitlab.candicey-weave:zenith-core:a79d983ce24181707b3f1c73a8152cf48f22de1c")

//    compileOnly(files("../Weave-Loader/build/libs/Weave-Loader-0.1.0.jar"))
//    compileOnly(files("../Celestial-Core/build/libs/Zenith-Core-0.2.0.jar"))

//    compileOnly("org.ow2.asm:asm:9.4")
//    compileOnly("org.ow2.asm:asm-tree:9.4")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "16"
}

tasks.jar {
    val wantedJar = listOf("viaversion", "viabackward", "viarewind-core", "ViaLoadingBase")
    configurations["compileClasspath"]
        .filter { wantedJar.find { wantedJarName -> it.name.contains(wantedJarName) } != null }
        .forEach { file: File ->
            from(zipTree(file.absoluteFile)) {
                this.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        }
}